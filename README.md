# QLearning

## Zadanie

Proszę zaimplementować algorytm Q-Learning i użyć go do wyznaczenia polityki decyzyjnej dla problemu [FrozenLake8x8-v0](https://gym.openai.com/envs/FrozenLake8x8-v0/) (w wersji domyślnej, czyli z włączonym poślizgiem). W problemie chodzi o to, aby agent przedostał się przez zamarznięte jezioro z pozycji 'S' do pozycji 'G' unikając punktów 'H'.

Oprócz zbadania domyślnego sposobu nagradzania (1 za dojście do celu, 0 w przeciwnym przypadku) proszę zaproponować własny system nagród i kar, po czym porównać osiągane wyniki z wynikami systemu domyślnego.

Za wynik uznajemy procent dojść do celu w 1000 prób. W każdej próbie można wykonać maksymalnie 200 akcji.


## Sposoby nagradzania

Zastosowano dwa sposoby nagradzania:
- domyślny: (1 za dojście do celu, 0 w przeciwnym przypadku)
- własny: 100 za dojście do celu, -100 za wpadnięcie w dziurę, -2 za wejście w ścianę, -1 w pozostałych przypadkach

## Wyniki

Średni procent dojść do celu, przeprowadzono po 5 testów na każdą statystykę

| Sposób nagradzania  | 100 epizodów | 1000 epizodów | 10000 epizodów | 50000 epizodów |
| ------------------- | ------------ | ------------- | -------------- | -------------- |
| domyślny | 0.3 | 4.1 | 21.2 | 47 |
| własny | 7 | 14.2 | 42.8 | 44.3 |


## Obserwacje i wnioski

- prosta implementacja algorytmu
- algorytm jest uniwersalny dla różnych problemów
- dla uzyskania najlepszych wyników potrzebne jest odpowiednie dobranie parametrów
- dla tych samych parametrów wynik dojść do celu wahał się nawet od 5 do 50 procent dla różnych testów
- przy trenowaniu bardzo ważna jest poprawność danych 
- przy domyślnym systemie nagradzania algorytm uczy się dużo wolniej, jednak przy 50000 epizodach daje już lepszy wynik od mojego systemu nagradzania
- możliwe jest, że poprzednia obserwacja jest błędna i wynika ze słabo dobranych parametrów
