from random import random, randint, choice
import gym

MAP_HEIGHT = 8
MAP_WIDTH = 8


class QLearning:
    def __init__(self, state_number, action_number):
        self.q_values = [[0 for j in range(action_number)]
                         for i in range(state_number)]
        self.action_number = action_number
        self.current_state = 0
        self.step = 1
        self.beta = 0.001
        self.gamma = 0.9
        self.epsilon = 0.1

    def chooseAction(self):
        if random() > self.epsilon:
            return self.chooseBestAction()
        else:
            return randint(0, self.action_number - 1)

    def chooseBestAction(self):
        return choice(self._getCurrentStateActionsWithBiggestQValues())

    def updateQValue(self, action, next_state, reward, episode_ends):
        self.q_values[self.current_state][action] += self.beta * (reward + self.gamma *
             (0 if episode_ends else max(self.q_values[next_state])) - self.q_values[self.current_state][action])

    def performAction(self, action, new_state, reward, episode_ends):
        self.updateQValue(action, new_state, reward, episode_ends)
        self.current_state = new_state
        self.step += 1

    def _getCurrentStateActionsWithBiggestQValues(self):
        res = []
        max_value = max(self.q_values[self.current_state])
        for i in range(self.action_number):
            if self.q_values[self.current_state][i] == max_value:
                res.append(i)
        return res

    def setState(self, state):
        self.current_state = state

    def reset(self):
        self.current_state = 0
        self.step = 1


def learn(q, env, rewarding_type="default"):
    env.reset()
    q.reset()

    done = False
    prev_observation = 0

    while not done:
        action = q.chooseAction()
        observation, reward, done, _ = env.step(action)

        if rewarding_type == "custom":
            reward = (reward - 1) * 2 + 1
            if observation == prev_observation:
                reward -= 1
            if done:
                reward *= 100

        q.performAction(action, observation, reward, done)
        prev_observation = observation


def run_test(q, env, step_limit=200):
    q.reset()
    env.reset()

    for _ in range(step_limit):
        action = q.chooseBestAction()
        observation, reward, done, _ = env.step(action)
        q.setState(observation)

        if done:
            return reward

    return 0

if __name__ == "__main__":
    env = gym.make('FrozenLake8x8-v1')
    q = QLearning(MAP_WIDTH * MAP_HEIGHT, env.action_space.n)

    for _ in range(50000):
        learn(q, env, "custom")

    print(sum([run_test(q, env) for _ in range(1000)]))

    env.close()
